class PublicController < ApplicationController
  respond_to :json ,:html
  
 
  
  
  
  def about
    Rails.cache.clear
  end
 
 
  def index
  #Rails.cache.clear
  @recent_videos = Video.all(:limit=>3)
  @news_pubs = Newspub.all(:limit=>4,:order=>'created_at DESC')
  @news_flash_images = News.find(:all,:conditions=>{:landing_page_image=>true},:limit=>6,:order=>'created_at')
  @tweeter =  get_tweets
  # Calendar Event Gem
   @month = (params[:month] || Time.zone.now.month).to_i
    @year = (params[:year] || Time.zone.now.year).to_i

    @shown_month = Date.civil(@year, @month)
    @event_strips = Event.find(:all,:conditions=>["start_date between ? and ?",Date.today, Date.today.next_month.beginning_of_month]) # Event.event_strips_for_month(@shown_month)
    
  end
 
  
  def contact
    @tweets = "" 
    @publics = Public.all
    @json = Public.all.to_gmaps4rails
    
   if request.post?
  
     if simple_captcha_valid? && params[:contact]["email"] && params[:contact]["fullname"] && params[:contact]["bdy"]
        @m = params[:contact]
       if PublicMailer.contact_us(@m).deliver
          flash[:notice] = "Thank you for contacting us. We will be in touch!"
          redirect_to root_path
          
        else
          flash[:error] = "Oops! Your message was not delivered due to some errors"
          render :contact
       end
       
       else
         
         flash[:error] = "Human Verification Failed, Please type the text displayed in the Captcha Box!!!"
          render :contact
     
     end
   end
    
    
  end
  def partners
    
  end
  
  def public disclaimer
    
  end
  
  def sitemap
    
  end
 
 def get_tweets
    if Rails.env.production?
    #Twitter::Client.new(oauth_token: "181478011-nDyLKSWDsqdpRCgvKVhUAyJPsqwXegJF6CXR7Yo", oauth_token_secret:"vqwuXdX8Amcr7mIk7bKvjN6zAiEOn2")
  
  @tweeter = Twitter.user_timeline("hakeemhal",:count=>10) 
  @tweets= Array.new
  @tweeter.each do |t|
  @tweets << t.text
   
   end
   session[:tweeter] = nil
   return @tweeter
   else 
     return  nil
  end
    end
end
