class ApplicationController < ActionController::Base
  protect_from_forgery
  #force_ssl
  include SessionsHelper
  include SimpleCaptcha::ControllerHelpers
  include CalendarHelper
  
  def login_required
     if signed_in?
      return true
    else
    flash[:warning]='Please login to continue'
    session[:return_to]=request.url
    #redirect_to :controller => "users", :action => "login"
    redirect_to "/login"
    return false 
    end
  end
  
  def check_user_group
    
    if (signed_in? && (current_user.group.downcase == 'admin') || signed_in? && (current_user.group.downcase == 'manager'))
      return true
    else 
      flash[:error] = "You do not have sufficient privileges to access that area"
      redirect_to  request.env["HTTP_REFERER"]
    end
  end
end
