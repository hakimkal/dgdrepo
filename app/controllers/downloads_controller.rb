class DownloadsController < ApplicationController
 before_filter :login_required, :only=>['new','destroy','edit','index','update','create']
 before_filter :check_user_group , :only=>['edit','new'] 
 
  def new
    @download = @model= current_user.downloads.build(params[:download])
  end

  def create
    @download = @model = current_user.downloads.build(params[:download])
    
    if @download.save
      flash[:notice] = "Successfully saved new download resource"
      redirect_to user_downloads_path current_user
      
    else
      flash[:error] = "Failed to save new download resource"
      render new_download_path
    end
  end

  def index
    if request.get? && !params[:user_id]
      @title = "All Downloads"
     @downloads  = Download.paginate(:page=>params[:page],:per_page =>10)
     #@downloads = {:download => {0=> {:id=>1,:title=>"Deejah"}}}
   
    else
      @title = "My Downloads"
      @downloads = @model = Download.where(:user_id=>params[:user_id]).paginate(:page=>params[:page], :per_page=>10)
    end
  
 end
  def destroy
    @newspub= Download.find_by_id(params[:id])
       if(@newspub && @newspub.user_id = current_user.id)
       @newspub.file = nil
       @newspub.save
       @newspub.destroy
       flash[:notice] = "Successfully deleted resource "
       redirect_to downloads_path  
     else
       flash[:error] = "Unable to delete  resource"
        redirect_to downloads_path
     end
  end

  def edit
    @download =@model= Download.find(:first,:conditions=>{:id => params[:id],:user_id=>params[:user_id]})
  end

  def update
    if request.put?
      
        @download = Download.find(:first,:conditions => {:id =>params[:user_id],:id=>params[:id]})
        if @download.update_attributes(params[:download])
        flash[:notice] = "Successfully Updated resource #{params[:download][:title]}"
        redirect_to  user_downloads_path current_user
        else
          flash[:error] = "unable to edit resource #{params[:download][:title]}"
        render edit_download_path params[:id] , params[:user_id]
        end
        
    end
  end

 

  def show
    redirect_to downloads_path
  end

  def view_all
    if request.get?
    @downloads = Download.paginate(:page =>params[:page],:per_page=>20)
    end
  end

end
