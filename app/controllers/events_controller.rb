class EventsController < ApplicationController
  
  
  before_filter :login_required, :only=>['new','destroy','edit','index','update','create']
  before_filter :check_user_group , :only=>['edit','new'] 
  def new
    
    @event = @model= current_user.events.build(params[:event])
  end

  def index
    if request.get? && params[:user_id]
      @title = "My Events"
    @events = Event.where(:user_id=>params[:user_id]).paginate(:page=>params[:page],:per_page=>20)
   
    else
      @title = "All Events"
      @events = Event.paginate(:page=>params[:page],:per_page=>1)
    
    
    end
  end

  def view_all
    @title = "All Events"    
    @events = Event.paginate(:page=>params[:page],:per_page=>10)
    
    
    
  end

  
  def edit
    @event = @model = Event.find_by_id(params[:id])
  end
  

  def update
    
     if request.put?
      
     @newspub = @model= Event.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:id]})
     if @newspub.nil?
       flash[:error] =" Invalid Selection detected. "
       redirect_to user_events_path current_user
       return nil
     end
       #find(:first,:conditions=>{"user_id"=>current_user.id,"id"=>params[:newspub][:id]})
      
      if (params[:event][:file])
              
       @newspub.file=nil
       @newspub.save
       
      end      
        
       params[:event][:user_id] = current_user.id
       # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
      
       if @newspub.update_attributes(params[:event])
       flash[:notice] = "Successfully Updated the Event: #{params[:event][:title]}"
       
       elsif !params[:event][:file]
        
         params[:event][:user_id] = current_user.id
         # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
         @newspub.update_attributes(params[:event])
         flash[:notice] = "Successfully Updated the Event  #{params[:event][:title]}"        
       else
         flash[:error] = "Failed to update the Event"  
      end
      
      redirect_to user_events_path current_user.id 
      end
  end

  def create
    if request.post?
      @newspub = @model = current_user.events.build(params[:event])
      if @newspub.save 
     flash[:notice]= "successfully saved a new  Event titled #{params[:event][:title]}"
     
      redirect_to user_events_path current_user.id
        
      else
     flash[:error]= "Some errors occured. We were unable to save uploaded file! Blank or Duplicates  files are not acceptable"
     # session[:new_model]= current_user.newspubs.build(params[:newspub]).errors
      #render :new
      render :new
      end
    end
  end

  def destroy
    
    @newspub=Event.find_by_id(params[:id])
       if(@newspub && @newspub.user_id = current_user.id)
       @newspub.file = nil
       @newspub.save
       @newspub.destroy
       flash[:notice] = "Successfully deleted the Event:  #{params[:event][:title]} "
       redirect_to nevents_path  
     else
       flash[:error] = "Unable to delete the Event:  #{params[:event][:title]}"
        redirect_to events_path
     end
  end

  def show
      @event = @model = Event.find_by_id(params[:id])
  end

end
