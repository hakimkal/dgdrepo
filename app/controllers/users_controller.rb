class UsersController < ApplicationController
  
  before_filter :login_required, :only=>['new','destroy','edit','change_password','index','show','update','create']
  before_filter :check_user_group , :only=>['edit','change_password','show','new'] 
    
  def new
     @user = @model= User.new
   end
                        
  def create
     
    @user = @model= User.new(params[:user])
    if @user.save
      flash.now[:notice] = "Successfully Saved a new user"
      redirect_to @user
    else
       
      flash.now[:error]= "Some errors occured. We were unable to create the user!"
      session[:new_model]= @user.errors
      render :new 
    end
  end
  
  def edit 
    
     @user = @model= User.find_by_id(params[:id])
  end
  
  def destroy
    
     if User.find_by_id(params[:id]).destroy
       flash.now[:notice] = "Successfully deleted user"
       redirect_to users_path  
     else
       flash.now[:error] = "Unable to delete user"
        redirect_to users_path
     end
    
  end
  def index
    @users = User.paginate(:page=>params[:page],:per_page=>20)
  end
  
  def change_password
     @user = @model= User.find_by_id(params[:id])
     if request.put? && User.find(params[:id])
       @user.id = current_user.id
       @user.password = params[:user][:password]
       @user.password_confirmation = params[:user][:password_confirmation]
       if @user.save(:validate=>false)
         flash[:notice] = "Password reset was successful"
         redirect_to myhome_path
       else
         flash[:error] = "Unable to reset password"
       end
     end
  end
  
  def edit_profile
    if current_user.id != params[:id]
    @user = @model= User.find_by_id(params[:id])  
   
   else
     flash[:error] = "Invalid user"
       
     redirect_to request.env["HTTP_REFERER"]
  end
  end
  
  def show 
    @user = User.find_by_id(params[:id])
  end
  
 def update
   
   @user = User.find_by_id(params[:id])
   
    
    if @user.update_attributes(params[:user])
     flash[:notice] = "#{params[:user][:firstname]}  #{params[:user][:lastname]}  has been updated successfully!"
     redirect_to users_path
    else
     @model=@user
      flash[:error] = "Unable to edit the user "
      if params[:user][:redirect_to].nil? 
        render :edit 
      else
        m = ""
        @user.errors.full_messages.each {|e| m += e + "\," }
         flash[:error] = m
     
       redirect_to edit_profile_user_path current_user
      end
    
   
   end
 end
 
 
end
