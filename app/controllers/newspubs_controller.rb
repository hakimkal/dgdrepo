class NewspubsController < ApplicationController
 
  before_filter :login_required, :only=>['new','destroy','edit','index','show','update','create']
  before_filter :check_user_group , :only=>['edit','show','new'] 
 
  def create
    if request.post?
      @newspub = @model = current_user.newspubs.build(params[:newspub])
      if @newspub.save 
     flash[:notice]= "successfully saved a new  #{params[:newspub][:category]}"
     
      redirect_to user_newspubs_path current_user.id
        
      else
     flash[:error]= "Some errors occured. We were unable to save uploaded file! Blank or Duplicates  files are not acceptable"
     # session[:new_model]= current_user.newspubs.build(params[:newspub]).errors
      #render :new
      #redirect_to request.env['HTTP_REFERER']     # flash[:error] = "An error occurred"
      render new_newspub_path 
      end
    end
  end

  def new
    @newspub = @model= current_user.newspubs.build(params[:newspub])
  end

  def index
    
    if request.get? && params[:user_id]
      @title = "My Uploads"
    @newspubs = Newspub.where(:user_id=>params[:user_id]).paginate(:page=>params[:page],:per_page=>20)
   
    else
      @title = "All Uploads"
      @newspubs = Newspub.paginate(:page=>params[:page],:per_page=>1)
    
    
    end
  end
  
  def view_all
    if request.get? && !params[:q]
    
    @newspubs = Newspub.paginate(:page=>params[:page],:per_page=>20)
    
    else
     
    @title = "Search Result(s) for Query -  '#{params[:q]}'"
    query = params[:q]
    category = params[:t]
    case category
    when "newsletter"
      @newspubs = Newspub.where("category = 'newsletter' and name ILIKE  ? ","#{query}%").paginate(:page=>params[:page],:per_page=>20)
    when "publications"
     @newspubs = Newspub.where("category = 'publications' and name ILIKE  ? ","#{query}%").paginate(:page=>params[:page],:per_page=>20)
    
    when "all"
     @newspubs = Newspub.where("name ILIKE  ? ","#{query}%").paginate(:page=>params[:page],:per_page=>20)
    
    end
    
    
    end
        
  end

  def show
  end

  def destroy
      @newspub= Newspub.find_by_id(params[:id])
       if(@newspub && @newspub.user_id = current_user.id)
       @newspub.file = nil
       @newspub.thumbfile = nil
       @newspub.save
       @newspub.destroy
       flash[:notice] = "Successfully deleted file "
       redirect_to newspubs_path  
     else
       flash[:error] = "Unable to delete file"
        redirect_to newspubs_path
     end
  end

  def update
    if request.put?
      
      @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
      #find(:first,:conditions=>{"user_id"=>current_user.id,"id"=>params[:newspub][:id]})
      
      if (params[:newspub][:file])
              
       @newspub.file=nil
       @newspub.save
       
       elsif (params[:newspub][:thumbfile])  
              
       @newspub.thumbfile=nil
       @newspub.save
       elsif(params[:newspub][:file] && params[:newspub][:thumbfile])
              
       @newspub.file=nil
       @newspub.thumbfile=nil
       @newspub.save
       end
       
       params[:newspub][:user_id] = current_user.id
       # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
       if @newspub.update_attributes(params[:newspub])
       flash[:notice] = "Successfully Updated the  #{params[:newspub][:category]}"
       
       elsif !params[:newspub][:file] && !params[:newspub][:thumbfile]
        
         params[:newspub][:user_id] = current_user.id
         # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
         @newspub.update_attributes(params[:newspub])
         flash[:notice] = "Successfully Updated the  #{params[:newspub][:category]}"        
       else
         flash[:error] = "Failed to update the item"  
      end
      
      redirect_to user_newspubs_path current_user.id    end
  end

  def edit
    @newspub = @model = Newspub.find_by_id(params[:id])
  end
 def feed
   
#   id                     :integer          not null, primary key
#  name                   :string(255)
#  description            :text
#  created_at             :datetime
#  updated_at             :datetime
#  file_file_name         :string(255)
#  file_content_type      :string(255)
#  file_file_size         :integer
#  file_updated_at        :datetime
#  user_id                :integer
#  category               :string(255)
#  thumbfile_file_name    :string(255)
#  thumbfile_content_type :string(255)
#  thumbfile_file_size    :integer
#  thumbfile_updated_at   :datetime
   @newspubs =  Newspub.all(:select => "id,name,description,file_file_name,file_file_size,thumbfile_file_name",:order=>'created_at DESC')

    respond_to do
      |format| format.rss{render :layout=>false} 
    end
 end
end
