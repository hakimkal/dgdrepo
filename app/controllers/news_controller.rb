class NewsController < ApplicationController
  before_filter :login_required , :only => ['new','index','create','update','edit','destroy']
  before_filter :check_user_group , :only => ['new','update','create','edit']
  def new
    @news=@model= current_user.news.build(params[:news])
  end

  def create
    
    if request.post?
      @news = @model = current_user.news.build(params[:news])
      if @news.save
        flash[:notice] = "You have successfully created a news item captioned:  #{params[:news][:caption]}"
       redirect_to user_news_index_path current_user 
       else
         flash[:errow] = "Errors occurred whilst saving"
         render new_news_path
      end
    end
  end

  def index
    if request.get? && !params[:user_id]
    @title = "All News"  
    
    @news=@model = News.paginate(:page=>params[:page],:per_page=>10)
    else
      @title = "My  Created News "  
    
    @news=@model = News.where(:user_id=>params[:user_id]).paginate(:page=>params[:page],:per_page=>10)
    end
  end

  def view_all
    @title = "All News"
    @news = News.paginate(:page=>params[:page],:per_page=>10)
    
  end

  def destroy
    
     @newspub= @model= News.find(params[:id])
       if(@newspub && (@newspub.user_id == current_user.id))
       @newspub.news_image = nil # To delete paperclip generated image
       @newspub.release = nil # To delete paperclip uploaded press_release
       
       @newspub.save
       @newspub.destroy
       flash[:notice] = "Successfully deleted the News Item:  #{@newspub.caption} "
       redirect_to news_index_path  
     elsif @newspub.nil?
       flash[:error] = "Unable to delete the News:  #{@newspub.caption} "
       redirect_to news_index_path 
     else
       flash[:error] = "Unable to delete the News:  #{@newspub.caption} "
        redirect_to news_path
     end
  end

  def edit 
    
    @news =@model= News.find(:first,:conditions=>{:user_id=>current_user.id,:id=>params[:id]})
    
    if params[:category] == 'press_release' 
     render :edit_press_release , :user_id => params[:user_id],:id => params[:id]
     
   
     
    end
  end

  def update
    if request.put?
       @news =@model= News.find(:first,:conditions=>{:user_id=>current_user.id,:id=>params[:id]})
      if params[:news][:news_image]
        @news.news_image= nil
        @news.save
         params[:news][:user_id] = current_user.id
       # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
         @news.update_attributes(params[:news])
         flash[:notice] = "Successfully Updated the  #{params[:news][:category]}"
         redirect_to news_index_path
      elsif !params[:news][:news_image]
        
         params[:news][:user_id] = current_user.id
         # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
          @news.update_attributes(params[:news])
         flash[:notice] = "Successfully Updated the  #{params[:news][:caption]}"   
         redirect_to news_index_path     
       else
         flash[:error] = "Failed to update the item"  
          render edit_news_path params[:id]
      end
    end
  end

  def show
    @news = @model = News.find_by_id(params[:id])
  end

def press_release
  @news = @model = current_user.news.build(params[:news])
end
  def edit_press_release
   @news =@model= News.find(:first,:conditions=>{:user_id=>current_user.id,:id=>params[:id]})
  end
end
