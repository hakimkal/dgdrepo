class PhotosController < ApplicationController
   
before_filter :login_required, :only=>['new','destroy','edit','index','show','update','create'],:except=>['view_all']
  before_filter :check_user_group , :only=>['edit','show','new'] 
  
  def new
    @photo = @model = current_user.photos.build(params[:photo])
  end
  
def create
    if request.post?
      @newspub = @model = current_user.photos.build(params[:photo])
      if @newspub.save 
     flash[:notice]= "successfully saved a new photo:  #{params[:photo][:title]} "
     
      redirect_to user_photos_path current_user.id
        
      else
     flash[:error]= "Some errors occured. We were unable to save uploaded file! Blank or Duplicates  are not acceptable"
     # session[:new_model]= current_user.newspubs.build(params[:newspub]).errors
     #    @newspub = @model =
      render new_photo_path 
     # redirect_to request.env['HTTP_REFERER']     # flash[:error] = "An error occurred"
      end
    end
  end

   
  def index
    
    if request.get? && params[:user_id]
      @title = "My Photos"
    @photos= Photo.where(:user_id=>params[:user_id]).paginate(:page=>params[:page],:per_page=>20)
   
    else
      @title = "All Photos"
      @photos = Photo.paginate(:page=>params[:page],:per_page=>1)
    
    
    end
  end
  
  def view_all
    if request.get?
    @title = "All Uploads"
    @photos = Photo.paginate(:page=>params[:page],:per_page=>20)
    
    end  
  end

  def show
  end

  def destroy
      @newspub= Photo.find_by_id(params[:id])
       if(@newspub && @newspub.user_id = current_user.id)
       @newspub.file = nil
       @newspub.save
       @newspub.destroy
       flash[:notice] = "Successfully deleted file "
       redirect_to photos_path  
     else
       flash[:error] = "Unable to delete file"
        redirect_to photos_path
     end
  end

  def update
    if request.put?
      
      @newspub = @model= Photo.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:photo][:id]})
      #find(:first,:conditions=>{"user_id"=>current_user.id,"id"=>params[:newspub][:id]})
      
      if (params[:photo][:file])
              
       @newspub.file=nil
       @newspub.save
       
       end
       
       params[:photo][:user_id] = current_user.id
       # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
       if @newspub.update_attributes(params[:photo])
       flash[:notice] = "Successfully Updated the  #{params[:photo][:category]}"
       
       elsif !params[:photo][:file]
        
         params[:photo][:user_id] = current_user.id
         # @newspub = @model= Newspub.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:newspub][:id]})
         @newspub.update_attributes(params[:photo])
         flash[:notice] = "Successfully Updated the  #{params[:photo][:photo]}"        
       else
         flash[:error] = "Failed to update the item"  
      end
      
      redirect_to user_photos_path current_user.id    end
  end

  def edit
    @newspub = @model = Photo.find_by_id(params[:id])
  end

end
