class FaqsController < ApplicationController
   before_filter :login_required , :only => ['new','index','create','update','edit','destroy']
   before_filter :check_user_group , :only => ['new','update','create','edit']

  def index
    if request.get? 
      if params[:user_id]
        
      @title = "My FAQs"
      @faqs = @model=Faq.where(:user_id=>params[:user_id]).paginate(:page=>params[:page],:per_page=>20)
   
      else
         @title = "All FAQs"
        @faqs =@model= Faq.paginate(:page=>params[:page],:per_page=>10)
    end
  end
  end
  
  def new
    @faq = @model =current_user.faqs.build(params[:faq])
  end

  def create
     
    if request.post?
      @faq = @model = current_user.faqs.build(params[:faq])
      if @faq.save
        flash[:notice] = "You have successfully created a new FAQ item:  #{params[:faq][:question]}"
       redirect_to user_faqs_path current_user 
       else
         flash[:errow] = "Errors occurred whilst saving"
         render new_faq_path
      end
    end
  end

  def edit
    @faq=@model= Faq.find(:first,:conditions=>{:id=>"#{params[:id]}",:user_id=>"#{params[:user_id]}"})
     
    
  end

  def destroy
    
     if Faq.find_by_id(params[:id]).destroy
       flash[:notice] = "Successfully deleted the Question"
       redirect_to faqs_path  
     else
       flash[:error] = "Unable to delete Question"
        redirect_to faqs_path
     end
  end

  def update
    @faq = @model= Faq.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:id]})
    
    if @faq.update_attributes(params[:faq])
     flash[:notice] = "#{params[:faq][:question]}  has been updated successfully!"
     redirect_to user_faqs_path current_user
    else
     @model=@faq
      flash[:error] = "Unable to edit question "
       
       redirect_to edit_user_faq_path current_user
      end
  end

  def view_all
    @faqs = @model = Faq.paginate(:page=>params[:page],:per_page=>10)
  end

  def show
    
  end

end
