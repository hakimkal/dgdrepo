class VideosController < ApplicationController
  
  
  before_filter :login_required, :only=>['new','destroy','edit','index','show','update','create'],:except=>['view_all']
  before_filter :check_user_group , :only=>['edit','show','new'] 
    
  def new
    @video = @model = Video.new if signed_in? 
    #@video = @model= current_user.videos.build(params[:video]) if signed_in? 
    
   end
                        
  def create
     
    @video = @model= current_user.videos.build(params[:video])
    if @video.save
      flash.now[:notice] = "Successfully Saved video"
      session[:new_model]= @video.errors
      redirect_to user_videos_path current_user.id
    else
      # @video = @model=Video.new(params[:video])
      flash[:error]= "Some errors occured. We were unable to save video!Blank or Duplicate Video Titles or Videos are not allowed"
      session[:new_model]= Video
      #render :new
      redirect_to request.env['HTTP_REFERER']
    end
  end
  
  def edit 
    
     @video = @model= Video.find(:first,:conditions=>{:user_id =>params[:user_id] ,:id =>params[:id]})
  end
  
  def destroy
    
     if Video.find_by_id(params[:id]).destroy
       flash[:notice] = "Successfully deleted user"
       redirect_to videos_path  
     else
       flash[:error] = "Unable to delete user"
        redirect_to videos_path
     end
    
  end
  
  def index
    if request.get? && params[:user_id]
    @title = "Your Videos"
    @videos = Video.where(:user_id=>params[:user_id]).paginate(:page=>params[:page],:per_page=>20)
    
    elsif request.get? && !params[:id]
      @title = "All Videos"
     @videos = Video.paginate(:page=>params[:page],:per_page=>20)
     
    end
    end
    
    def view_all
    if request.get? 
       @title = "All Videos"
       @videos = Video.paginate(:page=>params[:page],:per_page=>20)
     end
    end
      
  
  def show 
    
  end
  
 def update
  if request.put? 
      @video = @model= Video.find(:first,:conditions=>{:user_id =>current_user.id,:id =>params[:id]})
      if @video.update_attributes(params[:video])
       flash[:notice]="Successfully updated video "
       redirect_to videos_path
      else
        flash[:error]="failed to update video "
       redirect_to request.referer 
      end

  end
 end
 
end
