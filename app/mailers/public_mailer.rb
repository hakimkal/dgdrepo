class PublicMailer < ActionMailer::Base
  default from: "support@dgd.leproghrammeen.com"
  
  def contact_us(msg={})
    @message = msg
    mail(:from=>"#{msg["fullname"]}<#{msg["email"]}>",:cc=>"#{msg["email"]}",:subject=>"#{msg["subject"]}")
    
  end
end
