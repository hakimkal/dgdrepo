# == Schema Information
#
# Table name: publics
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  address    :string(255)
#  longitude  :float
#  latitude   :float
#  created_at :datetime
#  updated_at :datetime
#

class Public < ActiveRecord::Base
  attr_accessible :name , :address, :longitude,:latitude
  acts_as_gmappable
  
      def gmaps4rails_address
          address
      end
      
       def gmaps4rails_infowindow
         "<h4>#{name}</h4>" << "<h4>#{address}</h4>"
     end
end
