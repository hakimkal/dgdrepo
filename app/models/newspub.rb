# == Schema Information
#
# Table name: newspubs
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  description            :text
#  created_at             :datetime
#  updated_at             :datetime
#  file_file_name         :string(255)
#  file_content_type      :string(255)
#  file_file_size         :integer
#  file_updated_at        :datetime
#  user_id                :integer
#  category               :string(255)
#  thumbfile_file_name    :string(255)
#  thumbfile_content_type :string(255)
#  thumbfile_file_size    :integer
#  thumbfile_updated_at   :datetime
#

class Newspub < ActiveRecord::Base
  
 belongs_to :user
  attr_accessible :file , :thumbfile, :category,:name,:description
 
  has_attached_file :file  #, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :thumbfile  , :styles => { :medium => "300x300>", :thumb => "100x100>" }
  
  validates :category , :presence =>true
  validates :file, :attachment_presence => true
  validates_with AttachmentPresenceValidator, :attributes => :file

  
end
