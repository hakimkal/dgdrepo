# == Schema Information
#
# Table name: faqs
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  question   :text
#  answer     :text
#  created_at :datetime
#  updated_at :datetime
#

class Faq < ActiveRecord::Base
  
  belongs_to :user
  attr_accessible   :question,:answer,:user_id
  
  
  validates  :question,  :presence => true,
                         :uniqueness => {:case_sensitive=>false}
  
  validates  :answer,  :presence => true
                       
 
end
