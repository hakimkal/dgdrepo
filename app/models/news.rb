# == Schema Information
#
# Table name: news
#
#  id                      :integer          not null, primary key
#  caption                 :string(255)
#  news_content            :text
#  user_id                 :integer
#  author                  :string(255)
#  landing_page_image      :boolean
#  created_at              :datetime
#  updated_at              :datetime
#  release_file_name       :string(255)
#  release_content_type    :string(255)
#  release_file_size       :integer
#  release_updated_at      :datetime
#  news_image_file_name    :string(255)
#  news_image_content_type :string(255)
#  news_image_file_size    :integer
#  news_image_updated_at   :datetime
#  category                :string(255)
#

class News < ActiveRecord::Base
  belongs_to :user
  attr_accessible :release , :news_image, :category,:caption,:news_image ,:landing_page_image,:news_content , :author,:user_id
 
  has_attached_file :release  #, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  has_attached_file :news_image  , :styles => { :medium => "1000x1000>", :thumb => "100x100>" }
  
  validates :category , :presence =>true
 # validates :news_image, :attachment_presence => true
 #validates_with AttachmentPresenceValidator, :attributes => :news_image

end
