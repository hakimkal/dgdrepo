# == Schema Information
#
# Table name: downloads
#
#  id                :integer          not null, primary key
#  title             :string(255)
#  description       :text
#  user_id           :integer
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#

class Download < ActiveRecord::Base
  
  belongs_to :user 
  
   attr_accessible :title , :description , :file,:user_id
  
   has_attached_file :file  
   
  validates_with AttachmentPresenceValidator, :attributes => :file

  validates  :title,  :presence => true,
                      :length => {:maximum => 255},
                      :uniqueness => {:case_sensitive=>false}
  
  
end
