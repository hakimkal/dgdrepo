# == Schema Information
#
# Table name: events
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  title             :string(255)
#  description       :text
#  synopsis          :text
#  start_date        :datetime
#  end_date          :datetime
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#

class Event < ActiveRecord::Base
  
   belongs_to :user , :foreign_key=>:user_id
   attr_accessible :title ,:start_date,:end_date , :description , :file,:synopsis ,:user_id
  
   has_attached_file :file  , :styles => { :medium => "300x300>", :thumb => "100x100>" }
  
   has_event_calendar :start_at_field  => 'start_date', :end_at_field => 'end_date'
  
  validates :file, :attachment_presence => true
  validates_with AttachmentPresenceValidator, :attributes => :file

  validates  :title,  :presence => true,
                      :length => {:maximum => 255},
                      :uniqueness => {:case_sensitive=>false}
  
  validates  :start_date,  :presence => true
                        
 validates  :end_date,  :presence => true
                        
  
 
  validates  :description, :presence => true, 
                           :length=>{:minimum=>1}  #,:allow_blank=>true
  

end
