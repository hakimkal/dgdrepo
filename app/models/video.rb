# == Schema Information
#
# Table name: videos
#
#  id          :integer          not null, primary key
#  title       :string(255)
#  url         :string(255)
#  description :text
#  source      :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  user_id     :integer
#  location    :string(255)
#

class Video < ActiveRecord::Base
  belongs_to :user , :foreign_key=>:user_id
  attr_accessible :title ,:url , :description , :source ,:location,:user_id
  url_format = /\A(http:\/\/)+[\w*\-\d*\w+]+[\.\w+\-\d*]+[\w+\/]+[\w*]\z/i
 
  validates  :title,  :presence => true,
                      :length => {:maximum => 255},
                      :uniqueness => {:case_sensitive=>false}
  
  validates  :description,  :length=>{:minimum=>1},:allow_blank=>true
  
                         
  validates  :url,  :presence => true,
                    :format => {:with => url_format },
                    :uniqueness => {:case_sensitive=>false}
  
  validates  :location, :presence => true
  
  validates  :source,  :length=>{:minimum=>1},
                        :allow_blank=>true
                         
  #validates  :user_id,   :presence => true
                                                                            
 
end
