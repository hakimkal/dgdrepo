# == Schema Information
#
# Table name: photos
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  title             :string(255)
#  description       :text
#  created_at        :datetime
#  updated_at        :datetime
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#

class Photo < ActiveRecord::Base
  belongs_to :user , :foreign_key=>:user_id
  attr_accessible :title  ,:file, :description ,:user_id
  has_attached_file :file  , :styles => { :medium => "300x300>", :thumb => "100x100>" }
  
 validates :title , :presence =>true
  validates :file, :attachment_presence => true
  validates_with AttachmentPresenceValidator, :attributes => :file

end
