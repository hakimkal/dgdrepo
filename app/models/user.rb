# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  username           :string(255)
#  encrypted_password :string(255)
#  salt               :string(255)
#  firstname          :string(255)
#  lastname           :string(255)
#  email              :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  group              :string(255)
#

class User < ActiveRecord::Base
  has_many :videos
  has_many :newspubs
  has_many :photos
  has_many :events
  has_many :news
  has_many :faqs
  has_many :downloads
  attr_accessor :password, :updating_password
  attr_accessible :firstname ,:username , :lastname , :email,:password,:password_confirmation ,:group
  email_format = /\A[\w+\-\d.]+@[a-z\d\-.]+\.[a-z]+\z/i
  
  validates  :username,  :presence => true,
                         :length => {:maximum => 100},
                         :uniqueness => {:case_sensitive=>false}
  
  validates  :firstname,  :presence => true,
                         :length => {:maximum => 100}
                           
  validates  :lastname,  :presence => true,
                         :length => {:maximum => 100}
                         
  validates  :group,      :presence => true
                                                                            
  validates   :email ,   :presence => true,
                         :format => {:with => email_format },
                         :uniqueness => {:case_sensitive=>false}
                         
  validates  :password , :presence => true,
                         :confirmation =>true,
                         :length => {:within => 4..40},
                         :if => Proc.new { |u| !u.password.blank? || !u.password.nil? }
                         #:if => Proc.new { |user| !user.password.blank? || !user.password_confirmation.blank? || user.new_record?}

                         
                         
   
   
  before_save :encrypt_password,:if => Proc.new { |user| !user.password.blank? || !user.password_confirmation.blank? || user.new_record?}

  
  def has_password? submitted_password
    
    encrypted_password == encrypt(submitted_password)
    
  end
  
  def self.authenticate(s={})
       
     if s[:column] == 'email'
    @user = find_by_email(s[:username])
    else
    @user = find_by_username(s[:username])
    end
    
    if @user.nil?
    
    return nil
    elsif  @user.has_password?(s[:password])
        return @user
    else
      return nil
    end
  end
  
  def self.authenticate_with_salt(id,cookie_salt)
    user = find_by_id(id)
    (user && user.salt == cookie_salt) ? user : nil
  end
   
    
  def custom_update(model={})
    model.delete :password
    model.delete :password_confirmation
     update_attributes! model
     
  end
  
  
  private
  
  
  
 def make_salt  
     
    self.salt = Digest::SHA2.hexdigest("#{Time.now.utc}--#{password}")
  end
  
  def encrypt_password 
    self.salt = make_salt if new_record?
    self.encrypted_password = encrypt(password)
  end
  
  def encrypt str
   security_hash str
  end
  
  def security_hash str
    Digest::SHA2.hexdigest("#{self.salt}" + str)
  end
end
