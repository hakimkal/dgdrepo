xml.instruct! :xml, :version=>"1.0"
xml.rss :version =>"2.0" do
  xml.channel do 
    xml.title "DGD News and Publications Feed"
    xml.description "UNDP Nigeria DGD Syndicated Resources"
    xml.link view_all_newspub_url 0    
    for newspub in @newspubs
      xml.item do
        xml.title newspub.name
        xml.description newspub.description
        xml.pubDate newspub.created_at.to_s
        xml.link "http://" + request.host + newspub.file.url
        xml.guid newspub.file.url
      end
    end
  end
end

