Dgd::Application.routes.draw do
  
  resources :downloads do
    member do
      get 'view_all'
    end
  end
  resources :users do
    member do
      get 'change_password'
      put 'change_password'
      get 'edit_profile'
      put 'edit_profile'  
        end
    
    resources :downloads do
    member do
      get 'view_all'
    end
  end
   resources :faqs do
    member do
      get 'view_all'
    end
    end
  resources :news do
    member do
      get 'view_all'
      get 'press_release'
      get 'edit_press_release'
    end
  end
 resources :events do
     member do
       get 'view_all'
     end
   end
   resources :videos do
      
   end
   resources :newspubs do
     
   end
   resources :photos do
      
   end
  end
  
  resources :events do
     member do
       get 'view_all'
     end
   end
  resources :photos do
     member do
       get 'view_all'
     end
   end
  resources :videos do
     member do 
    get 'view_all'
    end
  end 
  resources :newspubs do
     member do
       get 'view_all'
     end
   end
  
  
    resources :faqs do
    member do
      get 'view_all'
    end
  end
  resources :news do
    member do
      get 'view_all'
      get 'press_release'
      get 'edit_press_release'
    end
  end
  match "/signup" => "users#new"  
  
 #match 'products/:id' => 'catalog#view'
 
  resources :sessions , :only=>[:create, :new,:destroy]
  get "sessions/destroy"
  match "/login" => "sessions#new"
  match "/admin" => "sessions#new"
  match "/admin" => "sessions#new"
  match "/logout" => "sessions#destroy"
  
  root to:"public#index"
  
  resources :public, :only=>[:index, :about,:contact,:partners,:sitemap]
  #match "/public" => 'public#index'
  # match "/public/" => 'public#index'
  match "public" =>'public#index'
  match "public/" =>'public#index'  
  match "/about" => "public#about"
  match "/contact" => "public#contact"
  match "/partners" => "public#partners"
  match "/disclaimer" => "public#disclaimer"
  match "/sitemap" => "public#sitemap"
  
  resources :dashboard, :only=>[:index] ,:via => 'get'
  match "/myhome" =>'dashboard#index'
  
  match '/feed' => 'newspubs#feed' ,
     :as => :feed, :defaults => { :format => 'rss' }
  #match '/calendar(/:year(/:month))' => 'calendar#index', :as => :calendar, :constraints => {:year => /\d{4}/, :month => /\d{1,2}/}
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end
   # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
