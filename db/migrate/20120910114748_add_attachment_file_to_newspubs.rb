class AddAttachmentFileToNewspubs < ActiveRecord::Migration
  def self.up
    change_table :newspubs do |t|
      t.has_attached_file :file
    end
  end

  def self.down
    drop_attached_file :newspubs, :file
  end
end
