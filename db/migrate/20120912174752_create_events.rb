class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :user_id
      t.string :title
      t.text :description
      t.text :synopsis
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
