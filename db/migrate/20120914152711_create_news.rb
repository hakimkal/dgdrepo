class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :caption
      t.text :news_content
      t.integer :user_id
      t.string :author
      t.boolean :landing_page_image

      t.timestamps
    end
  end
end
