class CreateNewspubs < ActiveRecord::Migration
  def change
    create_table :newspubs do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
