class AddAttachmentThumbfileToNewspubs < ActiveRecord::Migration
  def self.up
    change_table :newspubs do |t|
      t.has_attached_file :thumbfile
    end
  end

  def self.down
    drop_attached_file :newspubs, :thumbfile
  end
end
