class ChangeColumnsTypeEvents < ActiveRecord::Migration
  def up
    change_column :events ,:start_date, :datetime
    change_column :events ,:end_date, :datetime
  end

  def down
  end
end
