class AddUserIdToNewspubs < ActiveRecord::Migration
  def change
    add_column :newspubs, :user_id, :integer
  end
end
