class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :encrypted_password
      t.string :salt
      t.string :firstname
      t.string :lastname
      t.string :email

      t.timestamps
    end
  end
  
  def down
    drop_table :users
  end
end
