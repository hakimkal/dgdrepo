class AddAttachmentReleaseNewsImageToNews < ActiveRecord::Migration
  def self.up
    change_table :news do |t|
      t.has_attached_file :release
      t.has_attached_file :news_image
    end
  end

  def self.down
    drop_attached_file :news, :release
    drop_attached_file :news, :news_image
  end
end
