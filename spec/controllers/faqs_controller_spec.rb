require 'spec_helper'

describe FaqsController do
 render_views 
 
 before(:each) do
   @attrs = {:username=> "admin1",:password=> 
  "admin",:column=>'username',:firstname=>'Abdulhakim Haliru',:lastname=>"K",
  :group=>'admin',:email=>'hakimkal@gmail.com'}
 #User.create!(@attrs) 
 @chk = User.authenticate(@attrs)
    
  end
  describe "GET 'new'" do
    before(:each) do
      #test_sign_in(@chk)
    end
    it "returns http success" do
      get 'new',:user_id => 1
      #response.should be_success
     response.should include("<title>")
    end
  end

  describe "POST 'create'" do
    it "returns http success" do
    
      post 'create' , @params
      response.should be_success
    end
  end

  describe "GET 'edit'" do
    
   before(:each) do
     
   end
    it "returns http success" do
      get 'edit' ,:id => @params[:faq][:id],:user_id => @params[:faq][:user_id]
      response.body.should include('Edit')
    end
  end

  describe "GET 'destroy'" do
    it "returns http success" do
      get 'destroy'
      response.should be_success
    end
  end

  describe "GET 'update'" do
    it "returns http success" do
      get 'update'
      response.should be_success
    end
  end

  describe "GET 'view_all'" do
    it "returns http success" do
      get  :view_all ,:id=>0
      response.should be_success
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      get 'show'
      response.should be_success
    end
  end

end
