require 'spec_helper'

describe UsersController do
 attr={:username=>"admin1",:password=>"admin",:email=>"hakimkal@gmail.com",:password_confirmation=>"admin",:group=>'admin'} 
 #@user = User
 #before(:each) do 
 #  @user = UsersController.new
  # @user.create!(@attrs)
 #end
  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should_not be_success
    end
  end
  
  describe "GET 'edit'" do
    it "returns http success " do
       @user.should respond_to(:edit)
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should_not be_success
    end
  end

  describe "POST 'create'" do
    before(:each) do 
   @user = UsersController.new
   #@user.create!(@attrs)
 end
    it "returns http success" do
    post :create ,:params=>@attrs  
     @user.should respond_to(:create)
    end
  end

  describe "GET 'show'" do
    it "returns http success" do
      
      @user.should respond_to(:show)
    end
  end

  describe "DELETE 'destroy'" do
    it "returns http success" do
       
      @user.should respond_to(:destroy)
    end
  end

  describe "UPDATE 'update'" do
    it "returns http success" do
      
        @user.should respond_to(:update)
    end
  end

end
