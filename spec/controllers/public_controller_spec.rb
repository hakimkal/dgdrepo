require 'spec_helper'

describe PublicController do
 render_views
  
  describe "GET 'index'" do
  it "should be successful" do
    get 'index'
    response.should be_success
  end
  
  it "should have the right title" do
    get :index
    #response.should render_template(:index)
    response.body.should include("<title>Democratic Governance for Development (DGD) Project</title>")
  end
  end
  
   describe "GET 'about'" do
  it "should be successful" do
    get :about
    response.should be_success
    
  end
  end
  
   describe "GET 'contact'" do
  it "should be successful" do
    get :contact 
    response.should be_success
  end
  end
  
end
